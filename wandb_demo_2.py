#!/bin/env python
import argparse
from time import sleep
from random import random
from pprint import pprint
import wandb


def f(x):
    sleep(0.2)
    return x ** 4 - 10 * x ** 2 - 6 * x + random()


def df_dx(x):
    sleep(0.2)
    return 4 * x ** 3 - 20 * x - 6


def main(config):
    wandb.init()
    wandb.config.update(config)
    config = wandb.config

    values = dict()
    values['x']= config['starting_point']
    for i in range(config['iterations']):
        values['f'] = f(values['x'])
        values['df_dx'] = df_dx(values['x'])
        # pprint(values)
        wandb.log(values)
        values['x'] -= config['learning_rate'] * values['df_dx']


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Demo: find the min of a quadratic")
    parser.add_argument('--starting-point', type=float, default=0.0)
    parser.add_argument('--iterations', type=int, default=10)
    parser.add_argument('--learning-rate', type=float, default=1.0)
    args = parser.parse_args()
    main(vars(args))
