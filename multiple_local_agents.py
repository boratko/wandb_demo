#!/bin/env python
import argparse
import subprocess


def main(config):
    for i in range(config['num_agents']):
        subprocess.Popen(["wandb", "agent", config['sweep_id']])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('sweep_id', type=str)
    parser.add_argument('num_agents', type=int)
    args = parser.parse_args()

    main(vars(args))